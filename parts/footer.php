</div>
</div>

<footer class="footer blue--bg" id="footer">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-3 footer__item">
			 	<?php if ($title) : ?>
			 	<h5 class="footer__title"><?php echo esc_html($title); ?></h5>
			 	<?php endif; ?>

				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>

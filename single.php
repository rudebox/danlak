<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php 
    //hero img single post
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
  ?>

  <section class="page__hero padding--both" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">

  </section>

  <section class="page__content padding--top">
    <div class="wrap hpad">
      <h1 class="page__title"><?php echo esc_html(get_the_title()); ?></h1>
    </div>
  </section>
       

  <section class="single padding--bottom">

    <div class="wrap hpad">

      <article class="single__post" itemscope itemtype="http://schema.org/BlogPosting">

        <div itemprop="articleBody">
          <?php the_content(); ?>
        </div>

        <h5 class="single__cat">Kategori: <span><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?><span></h5>

      </article>

    </div>

  </section>

  <?php get_template_part('parts/gallery'); ?>  

</main>

<?php get_template_part('parts/footer'); ?>
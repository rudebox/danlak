<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>
  
  <section class="home padding--bottom">
    <div class="wrap hpad">
      <div class="row flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'offer' );?>

          <a href="<?php the_permalink(); ?>" class="col-sm-4 home__post" itemscope itemtype="http://schema.org/BlogPosting">

            <div class="home__wrap">
              <img class="home__thumb" src="<?php echo esc_url($thumb[0]); ?>);" alt="<?php echo esc_attr($thumb['alt']); ?>">
            </div>

            <header>
              <h2 class="home__title h3" title="<?php the_title_attribute(); ?>">              
                  <?php the_title(); ?>
              </h2>
            </header>

            <div class="home__excerpt" itemprop="articleBody">
              <?php the_excerpt(); ?>

              <span class="home__btn" href="<?php the_permalink(); ?>">Se mere</span>
            </div>

          </a>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>
      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>